<?php

include "valida.php";

$ano=date("Y");

$date=date("d/m/Y");
$dia=date("d");
$mes=date("m");
$semana=date("J");
$time=date("H:i:s");
include "includes/config_sistema.php";

$consulta = mysql_query("select * from franquia where usuario='$l'");

while($linha = mysql_fetch_object($consulta)) {
	
	
$nome = $linha->nome; 	
 	
$id = $linha->id;
$login = $linha->usuario;
$senha = $linha->senha;
$cidade = $linha->cidade;


}
?>

<!DOCTYPE html>
<html lang="pt">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Sistema Atendimento Kasa - <?php echo $cidade;?> </title>
  <!-- Font Awesome -->
  <link rel="shortcut icon" href="img/favicon.png" />
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <!-- Material Design Bootstrap -->
  <link href="css/mdb.min.css" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="css/style.min.css" rel="stylesheet">
  <!-- Toogle Menu -->
  <link href="css/toogle.css" rel="stylesheet">
  <style>

    .map-container{
overflow:hidden;
padding-bottom:56.25%;
position:relative;
height:0;
}
.map-container iframe{
left:0;
top:0;
height:100%;
width:100%;
position:absolute;
}
  </style>
</head>

<body class="grey lighten-3">

  <!--Main Navigation-->
  <header>

    <!-- Navbar -->
    <nav class="navbar  navbar-expand-lg navbar-light white scrolling-navbar">
      <div class="container">
        <div class="">
        <!-- Brand -->
        <a class="navbar-brand waves-effect" href="" target="_blank">
          <img class="mb-4 img-fluid" src="img/logo.png" alt="" width="85" height="72">
          
        </a>
        </div>
        <!-- Collapse -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
          aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <!-- Links nome de usuario -->
        <div class="collapse navbar-collapse" id="navbarSupportedContent">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                  <a class="nav-link" href="Home"><i class="fas fa-home"></i>Home </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link " href="" id="dropdown01" data-toggle="dropdown" aria-haspopup="" aria-expanded="false"><i class="fas fa-arrow-down"></i>Briefing</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                      <a class="dropdown-item" href="Cadastro"><i class="fas fa-user mr-3"></i>Novo Cadastro</a>
                      <a class="dropdown-item" href="Orcamento"><i class="fas fa-table mr-3"></i>Novo Orçamento</a>
                      <a class="dropdown-item" href="BuscaOrcamento"><i class="fas fa-map mr-3"></i>Buscar Orçamento</a>
                      <a class="dropdown-item" href="Status"><i class="fas fa-money-bill-alt mr-3"></i>Status</a>
                    </div>
                  </li>
                <li class="nav-item">
                  <a class="nav-link" href="http://bitly.com/kasamais" target="_blank"><i class="fas fa-calendar-check mr-3"></i>Eventos</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="https://api.whatsapp.com/send?phone=553538217488&text=Ol%C3%A1!%20Estou%20precisando%20de%20treinamento!" target="_blank"><i class="fas fa-graduation-cap mr-3 "></i>Treinamento</a>
                  </li>
               <li class="nav-item">
                  <a class="nav-link" href="Logout"><i class="fas fa-times mr-3"></i>Sair</a>
               </li>
                
              </ul>
        </div>

      </div>
      
    </nav>
    <!-- Navbar -->

    
  </header>
  <!--Main Navigation-->

  <!--Main layout-->
  
  <div class="container">
  <strong class="cor-verde-text ">SISTEMA KASA -  <?php
$meses = array (1 => "Janeiro", 2 => "Fevereiro", 3 => "Março", 4 => "Abril", 5 => "Maio", 6 => "Junho", 7 => "Julho", 8 => "Agosto", 9 => "Setembro", 10 => "Outubro", 11 => "Novembro", 12 => "Dezembro");
$diasdasemana = array (1 => "Segunda-Feira",2 => "Terça-Feira",3 => "Quarta-Feira",4 => "Quinta-Feira",5 => "Sexta-Feira",6 => "Sábado",0 => "Domingo");
$hoje = getdate();
$dia = $hoje["mday"];
$mes = $hoje["mon"];
$nomemes = $meses[$mes];
$ano = $hoje["year"];
$diadasemana = $hoje["wday"];
$nomediadasemana = $diasdasemana[$diadasemana];
echo "$nomediadasemana, $dia de $nomemes de $ano";
?></strong><br><br>
  <strong class="cor-verde-text"><?php echo $login;?> - <?php echo $cidade;?></strong>
  </div>  
  <main class="container">
  <div class="container-fluid mt-5">
    <!-- Heading -->
    <div class="card mb-4 wow fadeIn">
    <!--Card content- Painel Principal-->
    <div class="card-body d-sm-flex justify-content-between">
    <!--Grid column-->
    <div class="col-md-6 mb-4">
    <a href="https://drive.google.com/drive/folders/1f2BxhYrKnUjGYx11kU2_r3SEI5bghQ5f?usp=sharing" target="_blank"><img src="img/bancodeimagens.png" class="img-fluid"></a>
    </div>
    <div class="col-md-6 mb-4">
    <a href="https://www.instagram.com/kasaarquitetura" target="_blank"><img src="img/noticiaskasa.png" class="img-fluid"></a>
    </div>
    
    </div>
    <div class="card-body d-sm-flex justify-content-between">
        <!--Grid column-->
        <div>
        <a href=""><img src="img/solicitacoeskasa.png" class="img-fluid"></a>
        </div>
        <div class="col-md-6 mb-4">
        <a href="Orcamento"><img src="img/eventos.png" class="img-fluid"></a>
        </div>
        
        </div>
    
    </div>
   
     
</div>
</div>
<!-- Heading -->
</div>
    

    </div>
  </main>
  <!--Main layout-->

  <!--Footer-->
  <footer class="  text-center font-small verde-background darken-2 mt-4 wow fadeIn">

    <!--Call to action-->
    <div class="pt-4">
      <a class="btn btn-outline-black" href="https://drive.google.com/open?id=1fOVw5Y_O1amnucpvGXTzhU6x0K4oVppJ" target="_blank"
        role="button">Download Manuais
        <i class="fas fa-download ml-2"></i>
      </a>
      
    </div>
    <!--/.Call to action-->

    <hr class="my-4">
    <h6>Faça o download de todos os <strong>manuais/documentos</strong> franquia kasa</h6>
    <br>

    <div>

        
            
                <a href="https://www.facebook.com/kasaengenharia/" class="nav-link waves-effect" target="_blank">
                  <i class="fab fa-facebook-f icone"></i>
            
            
                <a href="https://www.instagram.com/kasaarquitetura" class="nav-link waves-effect" target="_blank">
                  <i class="fab fa-instagram icone"></i>
                  
                </a>
            
              
                <a href="https://open.spotify.com/user/22n7zo7447deikmsnjqjs2x6y/playlist/3twuuODwfxHtxFFnMPOpVR?si=pna4vCDsQweeH8t-kWObkA" class="nav-link waves-effect" target="_blank">
                  <i class="fab fa-spotify icone"></i>
                  
                </a>
              
                <a href="https://www.youtube.com/channel/UCR1xwRpZ84DScWiHZrRbBSQ" class="nav-link waves-effect" target="_blank">
                  <i class="fab fa-youtube icone"></i>
                  
                </a>
              
              
                <a href="https://www.linkedin.com/in/kasa-arquitetura-12a3a0178/" class=" nav-link waves-effect" target="_blank">
                  <i class="fab fa-linkedin-in "></i>
                 
                </a>
              
              
                <a href="https://api.whatsapp.com/send?phone=5535992221752&text=Ol%C3%A1%20tudo%20bem%3F%20Gostaria%20de%20ajuda%20no%20sistema%20da%20Kasa!" class="nav-link border border-light rounded waves-effect"
                  target="_blank">
                  
                  <i class="fab fa-whatsapp mr-2 icone"></i>Suporte
                 
                </a>
              
            
            
        </ul>
    </div>
    <!-- Right Icones -->


    <!--Copyright-->
    <div class="footer-copyright py-3">
      © <?php echo $ano; ?> Copyright:
       Todos os Direitos Reservados Sistema Kasa
    </div>
    <!--/.Copyright-->

  </footer>
  <!--/.Footer-->

  <!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Initializations -->
  <script type="text/javascript">
    // Animations initialization
    new WOW().init();

  </script>

  <!-- Charts -->
  <script>
    // Line
    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        datasets: [{
          label: '# of Votes',
          data: [12, 19, 3, 5, 2, 3],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    });

    //pie
    var ctxP = document.getElementById("pieChart").getContext('2d');
    var myPieChart = new Chart(ctxP, {
      type: 'pie',
      data: {
        labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
        datasets: [{
          data: [300, 50, 100, 40, 120],
          backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
          hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
        }]
      },
      options: {
        responsive: true,
        legend: false
      }
    });


    //line
    var ctxL = document.getElementById("lineChart").getContext('2d');
    var myLineChart = new Chart(ctxL, {
      type: 'line',
      data: {
        labels: ["January", "February", "March", "April", "May", "June", "July"],
        datasets: [{
            label: "My First dataset",
            backgroundColor: [
              'rgba(105, 0, 132, .2)',
            ],
            borderColor: [
              'rgba(200, 99, 132, .7)',
            ],
            borderWidth: 2,
            data: [65, 59, 80, 81, 56, 55, 40]
          },
          {
            label: "My Second dataset",
            backgroundColor: [
              'rgba(0, 137, 132, .2)',
            ],
            borderColor: [
              'rgba(0, 10, 130, .7)',
            ],
            data: [28, 48, 40, 19, 86, 27, 90]
          }
        ]
      },
      options: {
        responsive: true
      }
    });


    //radar
    var ctxR = document.getElementById("radarChart").getContext('2d');
    var myRadarChart = new Chart(ctxR, {
      type: 'radar',
      data: {
        labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Cycling", "Running"],
        datasets: [{
          label: "My First dataset",
          data: [65, 59, 90, 81, 56, 55, 40],
          backgroundColor: [
            'rgba(105, 0, 132, .2)',
          ],
          borderColor: [
            'rgba(200, 99, 132, .7)',
          ],
          borderWidth: 2
        }, {
          label: "My Second dataset",
          data: [28, 48, 40, 19, 96, 27, 100],
          backgroundColor: [
            'rgba(0, 250, 220, .2)',
          ],
          borderColor: [
            'rgba(0, 213, 132, .7)',
          ],
          borderWidth: 2
        }]
      },
      options: {
        responsive: true
      }
    });

    //doughnut
    var ctxD = document.getElementById("doughnutChart").getContext('2d');
    var myLineChart = new Chart(ctxD, {
      type: 'doughnut',
      data: {
        labels: ["Red", "Green", "Yellow", "Grey", "Dark Grey"],
        datasets: [{
          data: [300, 50, 100, 40, 120],
          backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
          hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
        }]
      },
      options: {
        responsive: true
      }
    });

  </script>

  <!--Google Maps-->
  <script src="https://maps.google.com/maps/api/js"></script>
  <script>
    // Regular map
    function regular_map() {
      var var_location = new google.maps.LatLng(40.725118, -73.997699);

      var var_mapoptions = {
        center: var_location,
        zoom: 14
      };

      var var_map = new google.maps.Map(document.getElementById("map-container"),
        var_mapoptions);

      var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "New York"
      });
    }

    new Chart(document.getElementById("horizontalBar"), {
      "type": "horizontalBar",
      "data": {
        "labels": ["Red", "Orange", "Yellow", "Green", "Blue", "Purple", "Grey"],
        "datasets": [{
          "label": "My First Dataset",
          "data": [22, 33, 55, 12, 86, 23, 14],
          "fill": false,
          "backgroundColor": ["rgba(255, 99, 132, 0.2)", "rgba(255, 159, 64, 0.2)",
            "rgba(255, 205, 86, 0.2)", "rgba(75, 192, 192, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(153, 102, 255, 0.2)", "rgba(201, 203, 207, 0.2)"
          ],
          "borderColor": ["rgb(255, 99, 132)", "rgb(255, 159, 64)", "rgb(255, 205, 86)",
            "rgb(75, 192, 192)", "rgb(54, 162, 235)", "rgb(153, 102, 255)",
            "rgb(201, 203, 207)"
          ],
          "borderWidth": 1
        }]
      },
      "options": {
        "scales": {
          "xAxes": [{
            "ticks": {
              "beginAtZero": true
            }
          }]
        }
      }
    });
    
	    $(document).ready(function(){
			$('#sidebarCollapse').on('click',function(){
				$('#sidebar').toggleClass('active');
			});
		});  
	</script>

  </script>
</body>

</html>
